import numpy as np
import torch
import copy

def clip_audio_valies(x,minv,maxv):
    x=torch.max(x,minv)
    x=torch.min(x,maxv)
    return x

def valid_bounds(audio,delta=0.3):
    audio=copy.deepcopy(np.asarray(audio))
    audio=audio.astype(np.float)

    valid_lb=np.zeros_like(audio,-0.3)
    valid_ub=np.zeros_like(audio,0.3)

    lb=audio-delta
    ub=audio+delta

    lb=np.maximum(valid_lb,np.maximum(lb,audio))
    ub=np.maximum(valid_ub,np.maximum(ub,audio))

    lb=lb.astype(np.float)
    ub=ub.astype(np.float)

    return lb,ub
