import torchaudio.transforms as transforms
import torchaudio.models as torch_models
import matplotlib.pyplot as plt
import numpy as np
import torch
import os


device = 'cuda' if torch.cuda.is_available() else 'cpu'

net=torch_models
r=torch.nn.Sequential.