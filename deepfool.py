import numpy as np
from torch.autograd import Variable
import torch
import copy
from torch.autograd.gradcheck import zero_gradients

def deepfool(audio,net,num_classes=10,overshoot=0.02,max_iter=50,lambda_fac=3., device='cuda'):
    audio=copy.deepcopy(audio)
    input_shape=audio.size()

    f_audio=net.forward(Variable(audio,requests_grad=True)).data.cpu().numpy().flatten()
    I=(np.array(f_audio)).flatten().argsort()[::-1]
    I=I[0:num_classes]
    label=I[0]

    pert_audio=copy.deepcopy(audio)
    r_tot=torch.zeros(input_shape).to(device)

    k_i=label
    loop_i=0

    while k_i == label and loop_i<max_iter:
        x=Variable(pert_audio,requires_grad=True)
        fs=net.forward(x)

        pert=torch.Tensor([np.inf])[0].to(device)
        w=torch.zeros(input_shape).to(device)

        fs[0, I[0]].backward(retain_graph=True)
        grad_orig = copy.deepcopy(x.grad.data)

